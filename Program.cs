﻿using Newtonsoft.Json;

class Input
{
    public int Count { get; set; }
    public int Paralleism { get; set; }
    public String SavePath { get; set; }
}

class Program
{
    static event Action<int, int> ProgressUpdated;

    static async Task Main()
    {
        Input input;

        try
        {
            input = JsonConvert.DeserializeObject<Input>(File.ReadAllText("./input.json"));
        }
        catch (FileNotFoundException)
        {
            Console.WriteLine("Input file not found.");
            return;
        }
        catch (Newtonsoft.Json.JsonException)
        {
            Console.WriteLine("Invalid input file.");
            return;
        }
        catch (Exception ex)
        {
            Console.WriteLine("An error occurred: " + ex.Message);
            return;
        }

        if(!Directory.Exists(input.SavePath))
            Directory.CreateDirectory(input.SavePath);

        int currentFile = 0;
        object lockObject = new object();
        ProgressUpdated += (completedImages, totalImages) =>
        {
            Console.Clear();
            Console.WriteLine($"Progress: {completedImages}/{totalImages}");
        };

        var throttler = new SemaphoreSlim(input.Paralleism);
        var tasks = new List<Task>();
        var client = new HttpClient();
        foreach (var image in GetImagesInput(input.Count))
        {
            await throttler.WaitAsync();
            tasks.Add(
                Task.Run(async () =>
                {
                    try
                    {
                        string localFilePath = Path.Combine(input.SavePath, image.name);
                        using (FileStream fileStream = new FileStream(localFilePath, FileMode.Create, FileAccess.Write))
                        {
                            var response = await client.GetAsync(image.url);
                            await response.Content.CopyToAsync(fileStream);
                        }

                        lock (lockObject)
                        {
                            currentFile += 1;
                            ProgressUpdated?.Invoke(currentFile, input.Count);
                        }
                    }
                    finally
                    {
                        throttler.Release();
                    }
                })
            );
        }

        try
        {
            await Task.WhenAll(tasks);
        }
        catch (System.Exception ex)
        {
            Console.WriteLine("An error occurred: " + ex.Message);
        }
    }


    static List<(string url, string name)> GetImagesInput(int count)
    {
        var random = new Random();

        var urls = new List<(string url, string name)>();

        for (int i = 0; i < count; i++)
            urls.Add(($"https://picsum.photos/{random.Next(1, 900)}/{random.Next(1, 900)}", $"{i + 1}.png"));

        return urls;
    }
}